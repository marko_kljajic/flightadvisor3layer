﻿using AutoMapper;
using FlightAdvisor.BusinessLogicLayer.Models.City;
using FlightAdvisor.BusinessLogicLayer.Models.Comment;
using FlightAdvisor.BusinessLogicLayer.Models.User;
using FlightAdvisor.DataAccessLayer.Entities;

namespace FlightAdvisor
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserRegisterResponseDTO>();
            CreateMap<UserRegisterDTO, User>();
            CreateMap<CityDTO, City>();
            CreateMap<AddCommentDTO, Comment>();
            CreateMap<UpdateCommentDTO, Comment>();
        }
    }
}
