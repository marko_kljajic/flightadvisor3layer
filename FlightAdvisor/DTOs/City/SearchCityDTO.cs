﻿namespace FlightAdvisor.DTOs.City
{
    public class SearchCityDTO
    {
        public string Name { get; set; }
        public int? NumberOfComments { get; set; }
    }
}
