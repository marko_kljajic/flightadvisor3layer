﻿namespace FlightAdvisor.DTOs.User
{
    public class UserRegisterResponseDTO
    {
        public int UserId { get; set; }
        public string Username { get; set; }
    }
}
