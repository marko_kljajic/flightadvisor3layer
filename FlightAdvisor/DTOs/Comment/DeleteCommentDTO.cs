﻿namespace FlightAdvisor.DTOs.Comment
{
    public class DeleteCommentDTO
    {
        public int Id { get; set; }
    }
}
