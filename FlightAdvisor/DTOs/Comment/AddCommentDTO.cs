﻿namespace FlightAdvisor.DTOs.Comment
{
    public class AddCommentDTO
    {
        public string Description { get; set; }
        public int CityID { get; set; }
    }
}
