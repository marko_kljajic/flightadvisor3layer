﻿using FlightAdvisor.BusinessLogicLayer.Contracts;
using FlightAdvisor.BusinessLogicLayer.Models.Comment;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace FlightAdvisor.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize(Roles = "User")]
    public class CommentController : ControllerBase
    {
        public CommentController(ICommentService commentService)
        {
            CommentService = commentService;
        }

        public ICommentService CommentService { get; }

        [HttpPost]
        public async Task<IActionResult> AddComment(AddCommentDTO addCommentDTO)
        {
            var response = await CommentService.AddComment(addCommentDTO);
            if (!response.Success) return BadRequest(response.Message);
            return Ok(response);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateComment(UpdateCommentDTO updateCommentDTO)
        {
            var response = await CommentService.UpdateComment(updateCommentDTO);
            if (!response.Success) return BadRequest(response.Message);
            return Ok(response);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteComment(DeleteCommentDTO deleteCommentDTO)
        {
            var response = await CommentService.DeleteComment(deleteCommentDTO);
            if (!response.Success) return BadRequest(response.Message);
            return Ok(response);
        }
    }
}
