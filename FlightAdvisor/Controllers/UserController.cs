﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using FlightAdvisor.BusinessLogicLayer.Contracts;
using FlightAdvisor.DataAccessLayer.Entities;
using FlightAdvisor.BusinessLogicLayer.Models.User;

namespace FlightAdvisor.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        public IUserService UserService { get; }

        public UserController(IUserService UserService)
        {
            this.UserService = UserService;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(UserRegisterDTO userRegisterDTO)
        {
            var response = await UserService.Register(userRegisterDTO);
            if (!response.Success) return BadRequest(response);
            return Ok(response);
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(UserLoginDTO userLoginDTO)
        {
            var response = await UserService.Login(userLoginDTO);

            if (!response.Success) return BadRequest(response);

            return Ok(response);
        }
    }
}
