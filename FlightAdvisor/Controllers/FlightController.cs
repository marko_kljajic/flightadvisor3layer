﻿using FlightAdvisor.BusinessLogicLayer.Contracts;
using FlightAdvisor.BusinessLogicLayer.Models.Flight;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace FlightAdvisor.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FlightController : ControllerBase
    {
        
        public FlightController(IFlightService flightService)
        {
            FlightService = flightService;
        }

        private IFlightService FlightService { get; }

        public async Task<IActionResult> Flight(FlightDTO flightDTO)
        {
            var response = await FlightService.Flight(flightDTO);

            if (!response.Success) return BadRequest(response.Message);

            return Ok(response);
        }
    }
}
