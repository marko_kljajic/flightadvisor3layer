﻿using FlightAdvisor.BusinessLogicLayer.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace FlightAdvisor.Controllers
{
    [ApiController]
    [Authorize(Roles = "Admin")]
    [Route("[controller]")]
    public class RouteController : ControllerBase
    {
        public RouteController(IRouteService routeService)
        {
            RouteService = routeService;
        }

        private IRouteService RouteService { get; }

        [HttpPost("import")]
        public async Task<IActionResult> Import()
        {
            var response = await RouteService.Import();

            if (!response.Success) return BadRequest(response.Message);

            return Ok(response);
        }
    }
}
