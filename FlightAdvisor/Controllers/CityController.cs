﻿using FlightAdvisor.BusinessLogicLayer.Contracts;
using FlightAdvisor.BusinessLogicLayer.Models.City;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace FlightAdvisor.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CityController : ControllerBase
    {
        public CityController(ICityService cityService)
        {
            CityService = cityService;
        }

        private ICityService CityService { get; }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> AddCity(CityDTO cityDTO)
        {
            var response = await CityService.AddCity(cityDTO);

            if (!response.Success) return BadRequest(response.Message);

            return Ok(response);
        }


        [Authorize(Roles = "User")]
        [HttpGet]
        public async Task<IActionResult> GetAll(NumberOfCommentsDTO numberOfComments)
        {
            var response = await CityService.GetAll(numberOfComments);

            if (!response.Success) return BadRequest(response.Message);

            return Ok(response);
        }

        [Authorize(Roles = "User")]
        [HttpGet("search")]
        public async Task<IActionResult> Search(SearchCityDTO searchCityDTO)
        {
            var response = await CityService.Search(searchCityDTO);

            if (!response.Success) return BadRequest(response.Message);

            return Ok(response);
        }
    }
}
