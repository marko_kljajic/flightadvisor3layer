﻿using FlightAdvisor.BusinessLogicLayer.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace FlightAdvisor.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize(Roles="Admin")]
    public class AirportController : ControllerBase
    {
        public AirportController(IAirportService airportService)
        {
            AirportService = airportService;
        }

        private IAirportService AirportService { get; }

        public async Task<IActionResult> Import()
        {
            var response = await AirportService.Import();

            if (!response.Success) return BadRequest(response.Message);

            return Ok(response);
        }
    }
}
