﻿using AutoMapper;
using FlightAdvisor.BusinessLogicLayer.Contracts;
using FlightAdvisor.BusinessLogicLayer.Entities;
using FlightAdvisor.DataAccessLayer.Entities;
using FlightAdvisor.DataAccessLayer.Interfaces;
using System;
using System.Configuration;
using System.Threading.Tasks;

namespace FlightAdvisor.BusinessLogicLayer.Services
{
    public class AirportService : IAirportService
    {
        public AirportService(IMapper mapper,ICityRepository cityRepository, IAirportRepository airportRepository)
        {
            Mapper = mapper;
            CityRepository = cityRepository;
            AirportRepository = airportRepository;
        }

        private IMapper Mapper { get; }
        public ICityRepository CityRepository { get; }
        public IAirportRepository AirportRepository { get; }

        public async Task<ServiceResponse<string>> Import()
        {
            var counter = 0;
            try
            {
                var fileName = ConfigurationManager.AppSettings["airports"];

                using (System.IO.StreamReader sourceFile = new System.IO.StreamReader(fileName))
                {

                    string line;
                    while ((line = sourceFile.ReadLine()) != null)
                    {
                        var tokens = line.Split(',');
                        for (int i = 0; i < tokens.Length; i++)
                        {
                            tokens[i] = tokens[i].Trim('\"');
                        }

                        if (await  CityRepository.CityExistsAsync(tokens[2].ToLower())  && !await AirportRepository.AirportExistsAsync(Int32.Parse(tokens[0]) ) ) 
                        {
                            var city = await CityRepository.GetCityByNameAsync(tokens[2]);

                            await AirportRepository.AddAirportAsync(new Airport()
                            {
                                AirportId = Int32.Parse(tokens[0]),
                                Name = tokens[1],
                                City = city,
                                IATA = tokens[4],
                                ICAO = tokens[5],
                                Latitude = Double.Parse(tokens[6]),
                                Longitude = Double.Parse(tokens[7]),
                                Altitude = Int32.Parse(tokens[8]),
                                Timezone = Int32.Parse(tokens[9]),
                                DST = tokens[10],
                                TZ = tokens[11],
                                Type = tokens[12],
                                Source = tokens[13]
                            });

                            counter++;
                        }
                    }
                }

                if (counter > 0)
                {
                    return new ServiceResponse<string>(true, $"You have successfuly added {counter} airport(s)!");
                }
            }
            catch (Exception ex)
            {
                return new ServiceResponse<string>(false, ex.Message);
            }

            return new ServiceResponse<string>(false, "You have already added all airports for current cities!");
        }
    }
}
