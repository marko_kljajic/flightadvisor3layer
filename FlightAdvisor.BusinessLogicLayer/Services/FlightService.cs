﻿using FlightAdvisor.BusinessLogicLayer.Contracts;
using FlightAdvisor.BusinessLogicLayer.Entities;
using FlightAdvisor.BusinessLogicLayer.Models.Flight;
using FlightAdvisor.DataAccessLayer.Entities;
using FlightAdvisor.DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightAdvisor.BusinessLogicLayer.Services
{
    public class FlightService : IFlightService
    {
        public FlightService(IRouteRepository routeRepository, IAirportRepository airportRepository)
        {
            RouteRepository = routeRepository;
            AirportRepository = airportRepository;
        }

        public IRouteRepository RouteRepository { get; }
        public IAirportRepository AirportRepository { get; }

        public async Task<ServiceResponse<Route>> Flight(FlightDTO flightDTO)
        {
            var sourceAirport = await AirportRepository.GetAirportByNameAsync(flightDTO.SourceAirportName);
            var destinationAirport = await AirportRepository.GetAirportByNameAsync(flightDTO.DestinationAirportName);
            try
            {
                var routeFromSource = await RouteRepository.GetRoutesForAirports(sourceAirport.AirportId, destinationAirport.AirportId);
                if (routeFromSource == null)
                {
                    return new ServiceResponse<Route>(false, "No routes for given cities!");
                }
                var cheapestFlight = routeFromSource.First();

                return new ServiceResponse<Route>(cheapestFlight, true, "We have found you a direct route to your destination!");
            }
            catch (Exception ex)
            {
                return new ServiceResponse<Route>(false, ex.Message);
            }
        }
    }
}
