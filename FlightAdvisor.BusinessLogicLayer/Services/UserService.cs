﻿using AutoMapper;
using FlightAdvisor.BusinessLogicLayer.Contracts;
using FlightAdvisor.BusinessLogicLayer.Entities;
using FlightAdvisor.BusinessLogicLayer.Models.User;
using FlightAdvisor.DataAccessLayer.Entities;
using FlightAdvisor.DataAccessLayer.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FlightAdvisor.BusinessLogicLayer.Services
{
    public class UserService : IUserService
    {
        public UserService(IUserRepository UserRepository,  IConfiguration configuration, IMapper mapper)
        {
            this.UserRepository = UserRepository;
            Configuration = configuration;
            Mapper = mapper;
        }

        private IUserRepository UserRepository { get; }
        private IConfiguration Configuration { get; }
        private IMapper Mapper { get; }

        public async Task<ServiceResponse<UserRegisterResponseDTO>> Register(UserRegisterDTO userRegisterDTO)
        {
            ServiceResponse<UserRegisterResponseDTO> response = new ServiceResponse<UserRegisterResponseDTO>();
            
            if (await UserRepository.UserExistsAsync(userRegisterDTO.Username) )
            {
                return new ServiceResponse<UserRegisterResponseDTO>(false, "User already exists!");
            }
            try
            {
                var user = Mapper.Map<User>(userRegisterDTO);

                CreatePasswordHash(userRegisterDTO.Password, out byte[] passwordHash, out byte[] passwordSalt);
                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;

                var result = await UserRepository.AddUserAsync(user);

                var responseUser = Mapper.Map<UserRegisterResponseDTO>(result);
                response.Data = responseUser;

                return new ServiceResponse<UserRegisterResponseDTO>(response.Data, true, $"Successfuly added new user: {user.Username}");
            }
            catch (Exception ex)
            {
                return new ServiceResponse<UserRegisterResponseDTO>(false, ex.Message);
            }
        }

        public async Task<ServiceResponse<string>> Login(UserLoginDTO userLoginDTO)
        {
            User user = await UserRepository.GetUserByUsernameAsync(userLoginDTO.Username);

            if (user == null)
            {
                return new ServiceResponse<string>(false, "User not found!");
            }
            else if (!VerifyPasswordHash(userLoginDTO.Password, user.PasswordHash, user.PasswordSalt))
            {
                return new ServiceResponse<string>(false, "Wrong password");
            }
            else
            {
                var response = CreateToken(user);
                return new ServiceResponse<string>(response, true, $"{user.FirstName} You have successfully loged in!");
            }
        }


        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.ASCIIEncoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != passwordHash[i]) return false;
                }
                return true;
            }
        }

        private string CreateToken(User user)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.UserId.ToString()),
                new Claim(ClaimTypes.Name, user.Username),
                new Claim(ClaimTypes.Role, user.Role)
            };

            SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration.GetSection("AppSettings:Token").Value));

            SigningCredentials creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = creds
            };

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}
