﻿using AutoMapper;
using FlightAdvisor.BusinessLogicLayer.Contracts;
using FlightAdvisor.BusinessLogicLayer.Entities;
using FlightAdvisor.BusinessLogicLayer.Models.Comment;
using FlightAdvisor.DataAccessLayer;
using FlightAdvisor.DataAccessLayer.Entities;
using FlightAdvisor.DataAccessLayer.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FlightAdvisor.BusinessLogicLayer.Services
{
    public class CommentService : ICommentService
    {
        public CommentService(DataContext context, IMapper mapper, IHttpContextAccessor httpContextAccessor, 
                            ICityRepository cityRepository, IUserRepository userRepository, ICommentRepository commentRepository)
        {
            Context = context;
            Mapper = mapper;
            HttpContextAccessor = httpContextAccessor;
            CityRepository = cityRepository;
            UserRepository = userRepository;
            CommentRepository = commentRepository;
        }

        public DataContext Context { get; }
        public IMapper Mapper { get; }
        public IHttpContextAccessor HttpContextAccessor { get; }
        public ICityRepository CityRepository { get; }
        public IUserRepository UserRepository { get; }
        public ICommentRepository CommentRepository { get; }

        private int GetUserId() => int.Parse(HttpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));

        public async Task<ServiceResponse<string>> AddComment(AddCommentDTO addCommentDTO)
        {
            var city = CityRepository.GetCityByIdAsync(addCommentDTO.CityId).Result;
            if (city != null)
            {
                try { 
                    var comment = Mapper.Map<Comment>(addCommentDTO);

                    comment.UserId = GetUserId();

                    await CommentRepository.AddAsync(comment);
                    await Context.SaveChangesAsync();

                    return new ServiceResponse<string>(true, $"You have succesfully added comment for city: {city.Name} !");
                }
                catch (Exception ex) 
                {
                    return new ServiceResponse<string>(false,ex.Message);
                }
            }
            return new ServiceResponse<string>(false, "City doesnt exist! ");
        }

        public async Task<ServiceResponse<string>> DeleteComment(DeleteCommentDTO updateCommentDTO)
        {
            try
            {
                User user = await UserRepository.GetUserByIdAsync(GetUserId());

                Comment commentForCheck = await CommentRepository.GetCommentByIdAsync(updateCommentDTO.CommentId);
                if (commentForCheck != null)
                {
                    if (commentForCheck.UserId != GetUserId())
                    {
                        return new ServiceResponse<string>(false, "You can't delete this comment beacuse you didn't create it!");
                    }

                    await CommentRepository.DeleteCommentAsync(commentForCheck);

                    return new ServiceResponse<string>(true, $"You have succesfully deleted comment with id: {commentForCheck.CommentId}!");
                }
                return new ServiceResponse<string>(false, "You can't delete comment that doesn't exist!");
            }
            catch (Exception ex)
            {
                return new ServiceResponse<string>(false, ex.Message);
            }
        }

        public async Task<ServiceResponse<string>> UpdateComment(UpdateCommentDTO updateCommentDTO)
        {
            try
            {
                Comment commentForUpdate = await CommentRepository.GetCommentByIdAsync(updateCommentDTO.CommentId);
                if (commentForUpdate != null)
                {
                    if (commentForUpdate.UserId != GetUserId())
                    {
                        return new ServiceResponse<string>(false, "You can't update this comment beacuse you didn't create it!");
                    }


                    commentForUpdate.Description = updateCommentDTO.Description;
                    commentForUpdate.TimeOfUpdate = updateCommentDTO.TimeOfUpdate;

                    await CommentRepository.UpdateAsync(commentForUpdate);

                    return new ServiceResponse<string>(true, $"You have succesfully updated comment with id: {commentForUpdate.CommentId}!");
                }
                return new ServiceResponse<string>(false, "You can't update comment because it doesn't exist!");
            }
            catch (Exception ex)
            {
                return new ServiceResponse<string>(false, ex.Message);
            }
        }
    }
}
