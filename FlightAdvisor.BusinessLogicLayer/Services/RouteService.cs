﻿using AutoMapper;
using FlightAdvisor.BusinessLogicLayer.Contracts;
using FlightAdvisor.BusinessLogicLayer.Entities;
using FlightAdvisor.DataAccessLayer.Entities;
using FlightAdvisor.DataAccessLayer.Interfaces;
using System;
using System.Configuration;
using System.Threading.Tasks;

namespace FlightAdvisor.BusinessLogicLayer.Services
{
    public class RouteService : IRouteService
    {
        public RouteService(IRouteRepository routeRepository, IAirportRepository airportRepository, IMapper mapper)
        {
            RouteRepository = routeRepository;
            AirportRepository = airportRepository;
            Mapper = mapper;
        }

        public IRouteRepository RouteRepository { get; }
        public IAirportRepository AirportRepository { get; }
        private IMapper Mapper { get; }

        public async Task<ServiceResponse<string>> Import()
        {
            var counter = 0;
            try
            {
                var fileName = ConfigurationManager.AppSettings["routes"];

                using (System.IO.StreamReader sourceFile = new System.IO.StreamReader(fileName))
                {
                    string line;
                    while ((line = sourceFile.ReadLine()) != null)
                    {
                        var tokens = line.Split(',');

                        if (tokens[3] == "\\N" || tokens[5] == "\\N")
                            continue;

                        if (await AirportRepository.AirportExistsAsync(Int32.Parse(tokens[3])))
                        {
                            if (await AirportRepository.AirportExistsAsync(Int32.Parse(tokens[5])))
                            {
                                await RouteRepository.AddAsync(new Route()
                                {

                                    AirlineCode = tokens[0],
                                    AirlineId = tokens[1],
                                    SourceAirportId = Int32.Parse(tokens[3]),
                                    DestinationAirportId = Int32.Parse(tokens[5]),
                                    Codeshare = tokens[6],
                                    Stops = Int32.Parse(tokens[7]),
                                    Equipment = tokens[8],
                                    Price = Double.Parse(tokens[9]),

                                });
                                counter++;
                            }
                        }
                    }
                }
                if (counter > 0)
                {
                    return new ServiceResponse<string>(true, "You have successfuly added route(s)!");
                }
            }
            catch (Exception ex)
            {
                return new ServiceResponse<string>(false, ex.Message);
            }
            return new ServiceResponse<string>(false, "You have already added all routes for current cities!");
        }
    }
}
