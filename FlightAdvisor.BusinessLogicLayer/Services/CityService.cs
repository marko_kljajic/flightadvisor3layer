﻿using AutoMapper;
using FlightAdvisor.BusinessLogicLayer.Contracts;
using FlightAdvisor.BusinessLogicLayer.Entities;
using FlightAdvisor.BusinessLogicLayer.Models.City;
using FlightAdvisor.DataAccessLayer.Entities;
using FlightAdvisor.DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FlightAdvisor.BusinessLogicLayer.Services
{
    public class CityService : ICityService
    {
        public CityService(ICityRepository cityRepository, IMapper mapper, ICommentRepository commentRepository)
        {
            CityRepository = cityRepository;
            Mapper = mapper;
            CommentRepository = commentRepository;
        }

        public IUserRepository UserRepository { get; }
        public ICityRepository CityRepository { get; }
        private IMapper Mapper { get; }
        public ICommentRepository CommentRepository { get; }

        public async Task<ServiceResponse<CityDTO>> AddCity(CityDTO cityDTO)
        {
            if (await CityRepository.CityExistsAsync(cityDTO.Name))
            {
                return new ServiceResponse<CityDTO>(false, "City already exists");
            }
            try
            {
                var city = Mapper.Map<City>(cityDTO);

                await CityRepository.AddCityAsync(city);

                return new ServiceResponse<CityDTO>(true, $"You have added a new city: {cityDTO.Name}!");
            }
            catch (Exception ex)
            {
                return new ServiceResponse<CityDTO>(false, ex.Message);
            }
        }

        public async Task<ServiceResponse<List<City>>> GetAll(NumberOfCommentsDTO numberOfComments)
        {
            ServiceResponse<List<City>> response = new ServiceResponse<List<City>>();
            try
            {
                if (numberOfComments.NumberOfComments == null)
                {
                    response.Data = await CityRepository.GetAllAsync();
                    var comments = await CommentRepository.GetAllAsync();
                }
                else if (numberOfComments.NumberOfComments == 0)
                {
                    response.Data = await CityRepository.GetAllAsync();
                }
                else
                {
                    response.Data = await CityRepository.GetAllAsync();
                    var comments = await CommentRepository.GettCustomNumberOfCommentsAsync((int)numberOfComments.NumberOfComments);
                }
                return new ServiceResponse<List<City>>(response.Data, true, "All cities and comments for each one.");
            }
            catch (Exception ex)
            {
                return new ServiceResponse<List<City>>(false, ex.Message);
            }
        }

        public async Task<ServiceResponse<City>> Search(SearchCityDTO searchCityDTO)
        {
            ServiceResponse<City> response = new ServiceResponse<City>();
            try
            {
                if (!await CityRepository.CityExistsAsync(searchCityDTO.Name))
                {
                    return new ServiceResponse<City>(false, "City doesn't exists!");
                }

                if (searchCityDTO.NumberOfComments == null)
                {
                    response.Data = await CityRepository.GetCityByNameAsync(searchCityDTO.Name);
                    var comments = await CommentRepository.GetAllAsync();
                }
                else if (searchCityDTO.NumberOfComments == 0)
                {
                    response.Data = await CityRepository.GetCityByNameAsync(searchCityDTO.Name);
                }
                else
                {
                    response.Data = await CityRepository.GetCityByNameAsync(searchCityDTO.Name);
                    var comments = await CommentRepository.GettCustomNumberOfCommentsAsync((int)searchCityDTO.NumberOfComments);
                }

                return new ServiceResponse<City>(response.Data, true, $"{searchCityDTO.Name} city and {searchCityDTO.NumberOfComments} comments for it!");
            }
            catch (Exception ex)
            {
                return new ServiceResponse<City>(false, ex.Message);
            }
        }
    }
}
