﻿namespace FlightAdvisor.BusinessLogicLayer.Entities
{
    public class ServiceResponse<T>
    {
        public ServiceResponse(T data, bool success, string message)
        {
            Data = data;
            Success = success;
            Message = message;
        }
        public ServiceResponse(bool success, string message)
        {
            Success = success;
            Message = message;
        }
        public ServiceResponse()
        {

        }

        public T Data { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
