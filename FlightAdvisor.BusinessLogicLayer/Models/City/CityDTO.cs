﻿using FlightAdvisor.DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlightAdvisor.BusinessLogicLayer.Models.City
{
    public class CityDTO
    {
        public string Name { get; set; }
        public string Country { get; set; }
        public string Description { get; set; }
        public List<DataAccessLayer.Entities.Comment> Comments { get; set; }
    }
}
