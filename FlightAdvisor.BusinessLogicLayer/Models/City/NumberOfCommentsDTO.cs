﻿namespace FlightAdvisor.BusinessLogicLayer.Models.City
{
    public class NumberOfCommentsDTO
    {
        public int? NumberOfComments { get; set; }
    }
}
