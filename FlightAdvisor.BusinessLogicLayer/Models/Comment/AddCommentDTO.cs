﻿namespace FlightAdvisor.BusinessLogicLayer.Models.Comment
{
    public class AddCommentDTO
    {
        public string Description { get; set; }
        public int CityId { get; set; }
    }
}
