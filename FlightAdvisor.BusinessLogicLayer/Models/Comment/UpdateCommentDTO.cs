﻿using System;

namespace FlightAdvisor.BusinessLogicLayer.Models.Comment
{
    public class UpdateCommentDTO
    {
        public int CommentId { get; set; }
        public string Description { get; set; }
        public DateTime TimeOfUpdate { get; set; } = DateTime.Now;
    }
}
