﻿namespace FlightAdvisor.BusinessLogicLayer.Models.Comment
{
    public class DeleteCommentDTO
    {
        public int CommentId { get; set; }
    }
}
