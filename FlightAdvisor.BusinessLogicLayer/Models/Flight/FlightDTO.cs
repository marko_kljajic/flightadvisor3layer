﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlightAdvisor.BusinessLogicLayer.Models.Flight
{
    public class FlightDTO
    {
        public string SourceAirportName { get; set; }
        public string DestinationAirportName { get; set; }
    }
}
