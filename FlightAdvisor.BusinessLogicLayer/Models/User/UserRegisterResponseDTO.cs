﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlightAdvisor.BusinessLogicLayer.Models.User
{
    public class UserRegisterResponseDTO
    {
        public int UserId { get; set; }
        public string Username { get; set; }
    }
}
