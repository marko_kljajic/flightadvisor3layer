﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlightAdvisor.BusinessLogicLayer.Models.User
{
    public class UserRegisterDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
