﻿using FlightAdvisor.BusinessLogicLayer.Entities;
using FlightAdvisor.BusinessLogicLayer.Models.Comment;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FlightAdvisor.BusinessLogicLayer.Contracts
{
    public interface ICommentService
    {
        Task<ServiceResponse<string>> AddComment(AddCommentDTO addCommentDTO);
        Task<ServiceResponse<string>> UpdateComment(UpdateCommentDTO updateCommentDTO);
        Task<ServiceResponse<string>> DeleteComment(DeleteCommentDTO updateCommentDTO);
    }
}
