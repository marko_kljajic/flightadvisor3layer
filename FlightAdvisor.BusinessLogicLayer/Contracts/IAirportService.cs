﻿using FlightAdvisor.BusinessLogicLayer.Entities;
using System.Threading.Tasks;

namespace FlightAdvisor.BusinessLogicLayer.Contracts
{
    public interface IAirportService
    {
        Task<ServiceResponse<string>> Import();
    }
}
