﻿using FlightAdvisor.BusinessLogicLayer.Entities;
using FlightAdvisor.BusinessLogicLayer.Models.City;
using FlightAdvisor.DataAccessLayer.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FlightAdvisor.BusinessLogicLayer.Contracts
{
    public interface ICityService
    {
        Task<ServiceResponse<CityDTO>> AddCity(CityDTO cityDTO);
        Task<ServiceResponse<List<City>>> GetAll(NumberOfCommentsDTO numberOfComments);
        Task<ServiceResponse<City>> Search(SearchCityDTO searchCityDTO);
    }
}
