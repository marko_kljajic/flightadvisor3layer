﻿using FlightAdvisor.BusinessLogicLayer.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FlightAdvisor.BusinessLogicLayer.Contracts
{
    public interface IRouteService
    {
        Task<ServiceResponse<string>> Import();
    }
}
