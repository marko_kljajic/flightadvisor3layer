﻿using FlightAdvisor.BusinessLogicLayer.Entities;
using FlightAdvisor.BusinessLogicLayer.Models.User;
using FlightAdvisor.DataAccessLayer.Entities;
using System.Threading.Tasks;

namespace FlightAdvisor.BusinessLogicLayer.Contracts
{
    public interface IUserService
    {
        Task<ServiceResponse<UserRegisterResponseDTO>> Register(UserRegisterDTO userRegisterDTO);
        Task<ServiceResponse<string>> Login(UserLoginDTO userLoginDTO);
    }
}
