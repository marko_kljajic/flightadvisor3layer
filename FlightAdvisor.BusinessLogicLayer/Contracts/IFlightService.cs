﻿using FlightAdvisor.BusinessLogicLayer.Entities;
using FlightAdvisor.BusinessLogicLayer.Models.Flight;
using FlightAdvisor.DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FlightAdvisor.BusinessLogicLayer.Contracts
{
    public interface IFlightService
    {
        Task<ServiceResponse<Route>> Flight(FlightDTO flightDTO);
    }
}
