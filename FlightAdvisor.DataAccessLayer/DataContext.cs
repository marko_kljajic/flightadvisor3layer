﻿using FlightAdvisor.DataAccessLayer.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlightAdvisor.DataAccessLayer
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
        public DbSet<User> Users { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Airport> Airports { get; set; }
        public DbSet<Route> Routes { get; set; }
        public DbSet<Comment> Comments { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // USER ENTITY
            modelBuilder.Entity<User>()
                .HasKey(u => u.UserId);

            //Properties
            modelBuilder.Entity<User>()
                .Property(u => u.FirstName)
                .IsRequired();

            modelBuilder.Entity<User>()
                .Property(u => u.LastName)
                .IsRequired();

            modelBuilder.Entity<User>()
                .Property(u => u.Username)
                .IsRequired();

            modelBuilder.Entity<User>()
                .Property(u => u.FirstName)
                .IsRequired();

            modelBuilder.Entity<User>()
                .Property(u => u.PasswordSalt)
                .IsRequired();

            modelBuilder.Entity<User>()
                .Property(u => u.PasswordHash)
                .IsRequired();

            modelBuilder.Entity<User>()
                .Property(u => u.Role)
                .IsRequired();

            //RELATIONSHIP with Comments   1:N
            modelBuilder.Entity<User>()
                .HasMany(u => u.Comments)
                .WithOne(c => c.User)
                .HasForeignKey(c => c.UserId);


            //Comment ENTITY
            modelBuilder.Entity<Comment>()
                .HasKey(c => c.CommentId);

            modelBuilder.Entity<Comment>()
                .Property(c => c.Description)
                .IsRequired();

            modelBuilder.Entity<Comment>()
                .Property(c => c.TimeOfCreation)
                .IsRequired();

            modelBuilder.Entity<Comment>()
                .Property(c => c.TimeOfUpdate)
                .IsRequired();

            //RELATIONSHIP with City  N:1
            modelBuilder.Entity<Comment>()
                .HasOne(com => com.City)
                .WithMany(city => city.Comments)
                .HasForeignKey(com => com.CityId);


            //City ENTITY
            modelBuilder.Entity<City>()
                .HasKey(c => c.CityId);

            modelBuilder.Entity<City>()
                .Property(c => c.Name)
                .IsRequired();
            modelBuilder.Entity<City>()
                .Property(c => c.Description)
                .IsRequired();
            modelBuilder.Entity<City>()
                .Property(c => c.Country)
                .IsRequired();

            //RELATIONSHIP with Airport   1:N
            modelBuilder.Entity<City>()
                .HasMany(c => c.Airports)
                .WithOne(a => a.City)
                .HasForeignKey(a => a.CityId);


            //Airport ENTITY
            modelBuilder.Entity<Airport>()
                .HasKey(a => a.AirportId);

            modelBuilder.Entity<Airport>()
                .Property(a => a.AirportId)
                .ValueGeneratedNever();

            modelBuilder.Entity<Airport>()
                .Property(a => a.Name)
                .IsRequired();
            modelBuilder.Entity<Airport>()
                .Property(a => a.Latitude)
                .IsRequired();
            modelBuilder.Entity<Airport>()
                .Property(a => a.Longitude)
                .IsRequired();
            modelBuilder.Entity<Airport>()
                .Property(a => a.Altitude)
                .IsRequired();
            modelBuilder.Entity<Airport>()
                .Property(a => a.Timezone)
                .IsRequired();
            modelBuilder.Entity<Airport>()
                .Property(a => a.DST)
                .IsRequired();
            modelBuilder.Entity<Airport>()
                .Property(a => a.TZ)
                .IsRequired();
            modelBuilder.Entity<Airport>()
                .Property(a => a.Type)
                .IsRequired();
            modelBuilder.Entity<Airport>()
                .Property(a => a.Source)
                .IsRequired();


            //Route ENTITY
            modelBuilder.Entity<Route>()
                .HasOne(r => r.SourceAirport)
                .WithMany(a => a.SourceAirportRoutes)
                .HasForeignKey(r => r.SourceAirportId)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<Route>()
                .HasOne(r => r.DestinationAirport)
                .WithMany(a => a.DestinationAirportRoutes)
                .HasForeignKey(r => r.DestinationAirportId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Route>()
                .HasKey(r => r.RouteId);

            modelBuilder.Entity<Route>()
                .Property(r => r.RouteId)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<Route>()
                .Property(r => r.AirlineCode)
                .IsRequired();

            modelBuilder.Entity<Route>()
                .Property(r => r.AirlineId)
                .IsRequired();

            modelBuilder.Entity<Route>()
                .Property(r => r.Stops)
                .IsRequired();

            modelBuilder.Entity<Route>()
                .Property(r => r.Price)
                .IsRequired();

            modelBuilder.Entity<Route>()
                .Property(r => r.Equipment)
                .IsRequired();
        }
    }
}
