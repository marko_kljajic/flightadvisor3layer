﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace FlightAdvisor.DataAccessLayer.Entities
{
    public class Airport
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AirportId { get; set; }
        public string Name { get; set; }

        public int CityId { get; set; }
        public City City { get; set; }

        public string IATA { get; set; } = null;
        public string ICAO { get; set; } = null;
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int Altitude { get; set; }
        public int Timezone { get; set; }
        public string DST { get; set; }
        public string TZ { get; set; }
        public string Type { get; set; }
        public string Source { get; set; }
        public List<Route> SourceAirportRoutes { get; set; }
        public List<Route> DestinationAirportRoutes { get; set; }
    }
}
