﻿namespace FlightAdvisor.DataAccessLayer.Entities
{
    public class Route
    {
        public int RouteId { get; set; }
        public string AirlineId{ get; set; }
        public string AirlineCode { get; set; }

        public Airport SourceAirport { get; set; }
        public int SourceAirportId { get; set; }

        public Airport DestinationAirport { get; set; }
        public int DestinationAirportId { get; set; }

        public string Codeshare { get; set; }
        public int Stops { get; set; }
        public string Equipment { get; set; }
        public double Price { get; set; }
    }
}
