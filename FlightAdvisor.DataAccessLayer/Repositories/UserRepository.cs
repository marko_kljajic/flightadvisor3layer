﻿using FlightAdvisor.DataAccessLayer.Interfaces;
using FlightAdvisor.DataAccessLayer.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FlightAdvisor.DataAccessLayer.Repositories
{
    public class UserRepository : IUserRepository
    {
        public UserRepository(DataContext context)
        {
            Context = context;
        }

        private DataContext Context { get; }

        public async Task<User> AddUserAsync(User user)
        {
            var response = await Context.Users.AddAsync(user);
            await Context.SaveChangesAsync();
            return response.Entity;
        }

        public async Task<User> GetUserByIdAsync(int id)
        {
            return await Context.Users.SingleOrDefaultAsync(u => u.UserId == id);
        }

        public async Task<User> GetUserByUsernameAsync(string username)
        {
            return await Context.Users.SingleOrDefaultAsync(u => u.Username == username); 
        }

        public async Task<bool> UserExistsAsync(string username)
        {
            return await Context.Users.AnyAsync(u => u.Username.ToLower() == username.ToLower());
        }

    }
}
