﻿using FlightAdvisor.DataAccessLayer.Entities;
using FlightAdvisor.DataAccessLayer.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightAdvisor.DataAccessLayer.Repositories
{
    public class CommentRepository : ICommentRepository
    {
        public CommentRepository(DataContext context)
        {
            Context = context;
        }

        public DataContext Context { get; }

        public async Task<Comment> AddAsync(Comment comment)
        {
         //   var res = Context.Comments.SingleOrDefault(c=> c.CommentId == comment.CommentId);
            var result = await Context.Comments.AddAsync(comment);
            await Context.SaveChangesAsync();
            return result.Entity;
        }

        public async Task DeleteCommentAsync(Comment comment)
        {
            Context.Remove(comment);
            await Context.SaveChangesAsync();
        }

        public async Task<List<Comment>> GetAllAsync()
        {
            return await Context.Comments.ToListAsync();
        }

        public Task<Comment> GetCommentByIdAsync(int commentId)
        {
            return Context.Comments.SingleOrDefaultAsync(c => c.CommentId == commentId);
        }

        public async Task<List<Comment>> GettCustomNumberOfCommentsAsync(int numberOfComments)
        {
            return await Context.Comments.Take(numberOfComments).ToListAsync();
        }

        public async Task<Comment> UpdateAsync(Comment comment)
        {
            var result = Context.Update(comment);
            await Context.SaveChangesAsync();
            return result.Entity;
        }
    }
}
