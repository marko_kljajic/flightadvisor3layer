﻿using FlightAdvisor.DataAccessLayer.Entities;
using FlightAdvisor.DataAccessLayer.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FlightAdvisor.DataAccessLayer.Repositories
{
    public class AirportRepository : IAirportRepository
    {
        public AirportRepository(DataContext context)
        {
            Context = context;
        }

        public DataContext Context { get; }

        public async Task AddAirportAsync(Airport airport)
        {
            await Context.Airports.AddAsync(airport);
            await Context.SaveChangesAsync();
        }

        public async Task<bool> AirportExistsAsync(int id)
        {
            return await Context.Airports.AnyAsync(a => a.AirportId == id);
        }

        public async Task<Airport> GetAirportByNameAsync(string name)
        {
            return await Context.Airports.FirstOrDefaultAsync(a => a.City.Name == name);
        }
    }
}
