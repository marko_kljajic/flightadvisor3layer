﻿using FlightAdvisor.DataAccessLayer.Entities;
using FlightAdvisor.DataAccessLayer.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightAdvisor.DataAccessLayer.Repositories
{
    public class RouteRepository : IRouteRepository
    {
        public RouteRepository(DataContext context)
        {
            Context = context;
        }

        public DataContext Context { get; }

        public async Task AddAsync(Route route)
        {
            await Context.AddAsync(route);
            await Context.SaveChangesAsync();
        }

        public async Task<List<Route>> GetRoutesForAirports(int sourceAirportId, int destinationAirportId)
        {
            return await Context.Routes.Where(r => r.SourceAirportId == sourceAirportId && r.DestinationAirportId == destinationAirportId)
                                        .OrderBy(r => r.Price)
                                        .ToListAsync();
        }
    }
}
