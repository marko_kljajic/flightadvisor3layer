﻿using FlightAdvisor.DataAccessLayer.Entities;
using FlightAdvisor.DataAccessLayer.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FlightAdvisor.DataAccessLayer.Repositories
{
    public class CityRepository : ICityRepository
    {
        public CityRepository(DataContext context)
        {
            Context = context;
        }

        public DataContext Context { get; }

        public async Task<City> AddCityAsync(City city)
        {
            var response = await Context.Cities.AddAsync(city);
            await Context.SaveChangesAsync();
            return response.Entity;
        }

        public async Task<List<City>> GetAllAsync()
        {
            var response = await Context.Cities.ToListAsync();
            return response;
        }
        public async Task<bool> CityExistsAsync(string name)
        {
            if (await Context.Cities.AnyAsync(c => c.Name.ToLower() == name.ToLower())) return true;

            return false;
        }

        public async Task<bool> CityExistsAsync(int cityId)
        {
            return await Context.Cities.AnyAsync(c => c.CityId == cityId);
        }

        public async Task<City> GetCityByNameAsync(string name)
        {
            return await Context.Cities.FirstOrDefaultAsync(c => c.Name == name);
        }

        public async Task<City> GetCityByIdAsync(int cityId)
        {
            var city = await Context.Cities.SingleOrDefaultAsync(c => c.CityId == cityId);
            return city;
        }
    }
}
