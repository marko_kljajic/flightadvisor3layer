﻿using FlightAdvisor.DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FlightAdvisor.DataAccessLayer.Interfaces
{
    public interface ICityRepository
    {
        Task<City> AddCityAsync(City city);
        Task<List<City>> GetAllAsync();
        Task<City> GetCityByNameAsync(string name);
        Task<City> GetCityByIdAsync(int cityId);
        Task<bool> CityExistsAsync(int cityId);
        Task<bool> CityExistsAsync(string name);
        //Task<City> Search(SearchCityDTO searchCityDTO);
    }
}
