﻿using FlightAdvisor.DataAccessLayer.Entities;
using System.Threading.Tasks;

namespace FlightAdvisor.DataAccessLayer.Interfaces
{
    public interface IUserRepository
    {
        Task<User> AddUserAsync(User user);
        Task<bool> UserExistsAsync(string username);
        Task<User> GetUserByUsernameAsync(string username);
        Task<User> GetUserByIdAsync(int id);
    }
}
