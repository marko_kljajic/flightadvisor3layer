﻿using FlightAdvisor.DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FlightAdvisor.DataAccessLayer.Interfaces
{
    public interface ICommentRepository
    {
        Task<List<Comment>> GetAllAsync();
        Task<List<Comment>> GettCustomNumberOfCommentsAsync(int numberOfComments);
        Task<Comment> GetCommentByIdAsync(int commentId);
        Task<Comment> AddAsync(Comment comment);
        Task DeleteCommentAsync(Comment comment);
        Task<Comment> UpdateAsync(Comment comment);
    }
}
