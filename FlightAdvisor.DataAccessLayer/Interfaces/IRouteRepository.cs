﻿using FlightAdvisor.DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FlightAdvisor.DataAccessLayer.Interfaces
{
    public interface IRouteRepository
    {
        Task AddAsync(Route route);
        Task<List<Route>> GetRoutesForAirports(int sourceAirportId, int destinationAirportId);
    }
}
