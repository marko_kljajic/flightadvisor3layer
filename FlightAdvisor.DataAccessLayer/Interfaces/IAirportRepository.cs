﻿using FlightAdvisor.DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FlightAdvisor.DataAccessLayer.Interfaces
{
    public interface IAirportRepository
    {
        Task<bool> AirportExistsAsync(int id);
        Task AddAirportAsync(Airport airport);
        Task<Airport> GetAirportByNameAsync(string name);
    }
}
